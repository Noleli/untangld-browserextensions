$(document).ready(init);

var entities;

function init()
{
    $("#name").html(urlParams["selection"]);
    
    /*entities = {12: {
            id: 12,
            type: "person",
            name: "Larry Birnbaum"
        },
        38: {
            id: 38,
            type: "person",
            name: "Paul Fussell"
        },
        39: {
            id: 39,
            type: "business",
            name: "chromeextension"
        },
        27: {
            id: 27,
            type: "person",
            name: "James Whitley"
        },
        30: {
            id: 30,
            type: "place",
            name: "2505 S. Walnut St., Springfield, IL 62704"
        }};*/

    $.post("http://untangld.noahliebman.com/api/backend.py", JSON.stringify({action: "get_entities"}), function(data) {
        entities = data;
        for(e in entities)
        {
            entities[e].type = entities[e].type.toLowerCase();
        }
        populate_entity_selector();
    });    
    
    $('#searchform').submit(function() {return false;});
    $('#search').keyup(populate_entity_selector);

    // $('select[multiple=multiple]').focus(function() {
    //     $(this).val($(this).children('option')[0]);
    // });

    $('#type').change(updateRelationSelector);
    $('#entity_selector').change(updateRelationSelector);

    $('#type').change(submittableStatus);
    $('#entity_selector').change(submittableStatus);
    $('#rel_type').change(submittableStatus);

    $(document).keydown(function(e) {
        if(e.which == 13 && $('input[type=text]:focus').length == 0 && !$("#add-btn").hasClass("disabled")) //enter, not in a textbox
        {
            doAdd();
        }
    });

    $('#add-btn').click(function() {
        if(!$("#add-btn").hasClass("disabled")) //enter, not in a textbox
        {
            doAdd();
        }
    });

    $('#rel_type').dblclick(function() {
        if(!$("#add-btn").hasClass("disabled")) //enter, not in a textbox
        {
            doAdd();
        }
    });

    
}

function populate_entity_selector()
{
    searchstring = $('#search').val().toLowerCase();
    entity_selector = $("#entity_selector");
    entity_selector.empty();
                
    for(var e in entities)
    {
        if(searchstring != "")
        {
            if(entities[e].name.toLowerCase().search(searchstring) != -1 || entities[e].type.toLowerCase().search(searchstring) != -1)
            {
                entity_selector.prepend('<option value="ent-' + entities[e].id + '">' + entities[e].name + '</option>');
            }
        }
        else
        {
            entity_selector.prepend('<option value="ent-' + entities[e].id + '">' + entities[e].name + '</option>');
        }
    }
}

function doAdd()
{
    // icon-map-marker, icon-user, icon-briefcase

    data = {};
    data["name"] = $('#name').text();
    data["type"] = $('#type').val();
    data["action"] = "add_entity";
    data = JSON.stringify(data);

    if($("#name").attr("data-id") == undefined)
    {
        $.post("http://untangld.noahliebman.com/api/backend.py", data, function(return_data){
            $("#name").attr("data-id", return_data.id);
            if($("#entity_selector").val() == null)
            {
                addRelationshipRow("Added &ndash; no relationship", "&nbsp;");
            }
            else if($('#rel_type').val() != null) // doing the if just for safety
            {
                addRelationship();
            }
        });
    }
    else if($('#rel_type').val() != null) // doing the if just for safety
    {
        addRelationship();
    }
    
    return false;
}

function addRelationship()
{
    new_id = $("#name").attr("data-id");
    rel_target_id = $("#entity_selector").val()[0].substr(4);

    data = {};
    data["fromID"] = new_id;
    data["toID"] = rel_target_id;
    data["relationship"] = $('#rel_type').val()[0];
    data["action"] = "add_relation";
    data = JSON.stringify(data);

    $.post("http://untangld.noahliebman.com/api/backend.py", data, function(return_data){
        addRelationshipRow(entities[$("#entity_selector").val()[0].substr(4)].name, $('#rel_type').val()[0], return_data.relationshipID);
    });
}

function addRelationshipRow(relname, reltype, relId)
{
    if(!relId) relId = undefined;
    if(relId != undefined)
    {
        // if there's a row in the table w/o a relationship, remove it.
        if($('#added-rels').find('tr[data-id="undefined"]').length > 0)
        {
            // there should never be more than 1, so I'm too lazy to .each()
            // it might be nice to slide this one out and slide the new one in or something
            $($('#added-rels').find('tr[data-id=undefined]')[0]).remove();
        }
    }
    newrow = $('<tr data-id="' + relId + '"><td>' + relname + '</td><td>' + reltype + '</td><td><a href="#del"><i class="icon-remove"></i></a></td></tr>');
    newrow.find('a[href=#del]').click(deleteRelationship);
    newrow.children('td').addClass('newflash');
    added = $("#added-rels").prepend(newrow);
    newrow.children('td').removeClass('newflash', 500);
}

function deleteRelationship()
{
    relId = $(this).parents('tr').attr("data-id");
    // if it's a relationship, remove it
    if(relId != "undefined")
    {
        data = {};
        data["relationshipID"] = relId;
        data["action"] = "delete_relationship";
        data = JSON.stringify(data);

        $.post("http://untangld.noahliebman.com/api/backend.py", data, function(return_data){
            $($('#added-rels').find('tr[data-id="'+relId+'"]')[0]).remove();
            if($('#added-rels').find('tr').length == 0)
            {
                addRelationshipRow("Added &ndash; no relationship", "&nbsp;");
            }
        });
    }
    else
    {
        // delete the entity
        data = {};
        data["entityID"] = $('#name').attr('data-id');
        data["action"] = "delete_entity";
        data = JSON.stringify(data);

        $.post("http://untangld.noahliebman.com/api/backend.py", data, function(return_data){
            $($('#added-rels').find('tr[data-id="'+relId+'"]')[0]).remove();
            if($('#added-rels').find('tr').length == 0)
            {
                addRelationshipRow("Entity removed", "&nbsp;");
            }
        });
    }
    // if it was the last relationship, replace it with a no-rel row
    // if it was a no-rel row, replace it with a (red?) entity deleted row
    //$(this).parents('tr').remove();
}

function submittableStatus()
{
    // xor: if one or the other selector is null, disable adding
    if(($('#entity_selector').val() == null || $('#rel_type').val() == null) && $('#rel_type').val() != $('#entity_selector').val())
    {
        $("#add-btn").addClass("disabled");
    }
    else
    {
        $("#add-btn").removeClass("disabled");
    }
}

function updateRelationSelector()
{
    new_type = $("#type").val();
    prev_type = null;
    if($("#entity_selector").val() != null)
    {
        prev_type = entities[$("#entity_selector").val()[0].substr(4)].type;
    }
    else
    {
        prev_type == null;
    }

    $("#rel_type").empty();

    // prev_type = person
    if(new_type == "person" && prev_type == "person")
    {
        $("#rel_type").append('<option value="Family">Family</option>');
        $("#rel_type").append('<option value="Boss">Boss</option>');
        $("#rel_type").append('<option value="Employee">Employee</option>');
        $("#rel_type").append('<option value="Coworker">Coworker</option>');
        $("#rel_type").append('<option value="Colleague">Colleague</option>');
        $("#rel_type").append('<option value="Student">Student</option>');
    }
    else if(new_type == "business" && prev_type == "person")
    {
        $("#rel_type").append('<option value="Receives funds from">Receives funds from</option>');
        $("#rel_type").append('<option value="Owns">Owns</option>');
        $("#rel_type").append('<option value="Employee">Employee</option>');
    }
    else if(new_type == "place" && prev_type == "person")
    {
        $("#rel_type").append('<option value="Rents">Rents</option>');
        $("#rel_type").append('<option value="Owns">Owns</option>');
        $("#rel_type").append('<option value="Owns &amp; occupies">Owns &amp; occupies</option>');
    }

    // prev_type = place
    if(new_type == "person" && prev_type == "place")
    {
        $("#rel_type").append('<option value="Rents">Rents</option>');
        $("#rel_type").append('<option value="Owns">Owns</option>');
        $("#rel_type").append('<option value="Owns &amp; occupies">Owns &amp; occupies</option>');
    }
    else if(new_type == "business" && prev_type == "place")
    {
        $("#rel_type").append('<option value="Located at">Located at</option>');
        $("#rel_type").append('<option value="Owns">Owns</option>');
    }
    else if(new_type == "place" && prev_type == "place")
    {
        $("#rel_type").append('<option value="Near">Near</option>');
        $("#rel_type").append('<option value="Adjacent to">Adjacent to</option>');
    }

    // prev_type = business
    if(new_type == "person" && prev_type == "business")
    {
        $("#rel_type").append('<option value="Owns">Owns</option>');
        $("#rel_type").append('<option value="Employee">Employee</option>');
    }
    else if(new_type == "business" && prev_type == "business")
    {
        $("#rel_type").append('<option value="Client of">Client of</option>');
        $("#rel_type").append('<option value="Subsidiary of">Subsidiary of</option>');
        $("#rel_type").append('<option value="Owns">Owns</option>');
        $("#rel_type").append('<option value="Alternate name for">Alternate name for</option>');
    }
    else if(new_type == "place" && prev_type == "business")
    {
        $("#rel_type").append('<option value="Located at">Located at</option>');
        $("#rel_type").append('<option value="Owns">Owns</option>');
    }

    // prev_type = municipality
    if(new_type == "person" && prev_type == "municipality")
    {
        $("#rel_type").append('<option value="Lives in">Lives in</option>');
        $("#rel_type").append('<option value="Elected official of">Elected official of</option>');
    }
}

// http://stackoverflow.com/a/2880929
var urlParams = {};
(function () {
    var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&=]+)=?([^&]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        q = window.location.search.substring(1);

    while (e = r.exec(q))
       urlParams[d(e[1])] = d(e[2]);
})();