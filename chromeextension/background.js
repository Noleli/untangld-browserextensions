function getClickHandler(info, tab)
{
    //chrome.extension.getBackgroundPage().console.log(JSON.stringify(info));
    chrome.windows.create({
        "url": "popup.html?selection=" + info.selectionText, //"data:text/html," + content,
        "type": "popup",
        "width": 500,
        "height": 350,
        "top": 100,
        "left": 150
        }
    );
    // window.document.appendChild("<p>hi</p>");
}

chrome.contextMenus.create({
    "title" : 'Add "%s" as an entity',
    "type" : "normal",
    "contexts" : ["selection"],
    "onclick" : getClickHandler
});